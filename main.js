const Calkulator = new function () {
    this.display = document.getElementById('display');
	this.elements = document.getElementsByTagName('button');

	this.addDisplayValue = function() {
		display.value  += this.innerHTML;
	   }

	this.calculate = function() {
        display.value = eval(display.value);
	}
 
    this.clearDisplay = function () {
    	display.value = '';
    } 

    this.clearDigit = function ()  {
		display.value = display.value.slice(0,-1);
	}

	for(let i=0; i<this.elements.length; i++)  {
		if(this.elements[i].innerHTML === '=')  {
			    this.elements[i].addEventListener('click', this.calculate);
		}  else if(this.elements[i].innerHTML === 'CE')  {
				this.elements[i].addEventListener('click', this.clearDisplay);
		}  else if(this.elements[i].innerHTML === 'C')  {
				this.elements[i].addEventListener('click', this.clearDigit);
		}  else  {
			   this.elements[i].addEventListener('click', this.addDisplayValue)
		   }
	}
	//console.log(display);
}